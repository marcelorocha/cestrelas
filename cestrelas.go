package main;

import "regexp"
import "fmt"
import "bufio"
import "os"
import "log"

var re *regexp.Regexp

func convertLine (line string) string {
	match := re.FindStringSubmatch(line)
	if match == nil {
		return ""	
	}
	value := match[7][2:] 
	ref :=  match[3]
	refFmt := ref[3:7] + "/" + ref[0:2]
	return fmt.Sprintf("%s;\"%s\";%s;%s\n", match[1], value, match[4], refFmt)
}

func convertFile(inputFile, outputFile string) {
	re = regexp.MustCompile(`(?:\d{5})\s+(\d{3,4}[CD])\s*-?\s?(.+?)\s+A1\s+([\w|/]{7})\s+([\w|/]{10})\s+([\w|/]{10})\s+(R\$\d+,\d{2})\s+(R\$\d+,\d{2})`)

	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	
	outfile, outerr := os.Create(outputFile)
	if outerr != nil {
		log.Fatal(outerr)
	}
	defer outfile.Close()
	
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		outline := convertLine(scanner.Text())
		if outline != "" {
			outfile.WriteString(outline)
		}
	}
}

func main() {
	
	cmd := os.Args[1]
	if cmd == "convert" {
		convertFile(os.Args[2], os.Args[3])	
	}
}